/*
* Chatbot by Lenitos
*
*/

/* jshint node: true, devel: true */
'use strict';

const
    bodyParser = require('body-parser'),
    config = require('config'),
    crypto = require('crypto'),
    express = require('express'),
   // http = require('http'), No redirect now
    https = require('spdy'),
    routes = require('./routes'),
    bot = require('./bot');

var fbVerify = {};
if (config.facebook.active){
    const   fb = require('./facebook'),
            Airline = require('./facebook/airline');

    //Create FB Messenger proxy
    const airline = new Airline(fb);

    fb.on('error', msg => console.error(msg));

    //Security check - is it fb post?
    fbVerify.verify =  fb.verifyRequestSignature;
}


//Start Express
var app = express();

app.set('port', config.get('port') || 5000);
app.use(bodyParser.json(fbVerify)); //this is done only for fb TODO: create special token checker
app.use(express.static('public'));
//TODO: For production remove default error handler, no stack to user
//import routes
app.use('/', routes);

//start SSL
const DOMAIN = config.get('domain');
const EMAIL = config.get('email');
var PORT = config.get('port');
var SSL_PORT = config.get('sslPort');

/* Note: using staging server url, remove .testing() for production
 Using .testing() will overwrite the debug flag with true
 Also, you'll need to remove .testing() and rm -rf '" + leConfDir + "'"
 */
var LEX = require('letsencrypt-express'); //.testing();

var leConfDir = require('os').homedir() + '/letsencrypt/etc';
var lex = LEX.create({
    configDir: leConfDir
    , approveRegistration: function (hostname, cb) { // leave `null` to disable automatic registration
        cb(null, {
            domains: [DOMAIN]
            , email: EMAIL // user@example.com
            , agreeTos: true
        });
    }
});

lex.onRequest = app;
//TODO: needs sudo for 443, fix this (nginx...)!!!
lex.listen([PORT], [SSL_PORT], function () {
    var protocol = ('requestCert' in this) ? 'https': 'http';
    console.log("Listening at " + protocol + '://localhost:' + this.address().port);
});


if (config.telegram.active){
    //Needed for telegram webhooks
    const certDir = leConfDir + '/live/' + DOMAIN;
//create telegram proxy
    let telegram = require('./telegram').get(certDir);

    telegram.on('text', function(msg) {

        const chatId = msg.chat.id;

        bot.reply(fromId, text, (err, reply)=>{
            if (err) return console.error(err);

            telegram
                .sendMessage(chatId, reply)
                .catch  ( err => console.error(err)); //promise
        });
    });
}

if (config.slack.active) {
    const   SLACK = require('./slack'),
            slack = SLACK.client,
            SLACK_EVENTS = SLACK.events;

    slack.on(SLACK_EVENTS.MESSAGE, message => {
        // Listens to all `message` events from the team
        console.log(message);
        slack.sendMessage('this is a test message', 'C0CHZA86Q', messageSent => {
            // optionally, you can supply a callback to execute once the message has been sent
        });
    });

    slack.on(SLACK_EVENTS.CHANNEL_CREATED,  (message) => {
        // Listens to all `channel_created` events from the team
        console.log(message);
    });
}


exports.app = app;



