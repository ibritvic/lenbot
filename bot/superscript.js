/**
 * Interface to bot dialog engine
 * superscript.js used
 */
'use strict';

var net             = require("net");
var superscript     = require("superscript");
var mongoose        = require("mongoose");
var facts           = require("sfacts");
var factSystem      = facts.create('telnetFacts');
var config          = require('config');

mongoose.connect(config.get('db'));
var options = {};

options['factSystem'] = factSystem;
options['mongoose'] = mongoose;

var _ready = false, bot; //init finished

module.exports = {
    init: () => {
        // Main entry point
        new superscript(options, (err, botInstance)=>{
            if (err) return; //TODO: Should log error here
            bot = botInstance;
            _ready = true;
        });
    },
    reply: (userId, message, cb)=> { //cb -> (err, reply)
        if(!_ready || !bot) return cb('Bot engine not initialized');
        // sanitize incoming messages.
        message = message.replace(/[\x0D\x0A]/g, "").trim();
        // UserId to monitor thread
        bot.reply(userId, message.trim(), cb);
    }
};


