const RtmClient = require('@slack/client').RtmClient,
      RTM_EVENTS = require('@slack/client').RTM_EVENTS,
      config = require('config'),
      token = config.get('slack').token;

var rtm = new RtmClient(token, {logLevel: 'debug'});
rtm.start();

exports.client = rtm;

exports.events = RTM_EVENTS;