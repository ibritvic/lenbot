"use strict";
const TelegramBot = require('node-telegram-bot-api'),
      config = require('config'),
      telegramConfig = config.get('telegram'),
      token =  telegramConfig.token,
      port  = telegramConfig.port;

//throw new Error('do not listen to this port, change facebook also and open ports on server')
exports.get = (dir) => {
    let options = {
        webHook: {
            port: port,
            key:  dir + '/privkey.pem',
            cert: dir + '/cert.pem'
        }
    };

    var telegram = new TelegramBot(token, options);
    //Controller?
    telegram.setWebHook(`https://bot.lenitos.com:${port}/tg/hook`, dir + '/cert.pem');

    return telegram;

//Use `ngrok http 8080` to tunnels localhost to a https endpoint. Get it at https://ngrok.com/
//telegram.setWebHook('https://_____.ngrok.io/' + token);
};