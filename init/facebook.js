/**
 * Set facebook messanger app through POST-s
 * @type {request|exports|module.exports}
 */
"use strict";
const request = require('request'),
      config = require('config'),
      facebook = config.get('facebook'),
      PAGE_ACCESS_TOKEN = facebook.pageAccessToken,
      MENU_URL = facebook.menuUrl,
      url = `https://graph.facebook.com/v2.6/me/thread_settings?access_token=${PAGE_ACCESS_TOKEN}`;


exports.menu = () => {
  let data = {
    "setting_type" : "call_to_actions",
    "thread_state" : "existing_thread",
    "call_to_actions" : [
        {
            "type": "postback",
            "title": "Check-in",
            "payload": JSON.stringify({type:'checkin', bookingNumber: 'D3WSA110R'})
        },
        {
            "type": "postback",
            "title": "Boarding pass",
            "payload": JSON.stringify({type:'boarding', bookingNumber: 'D3WSA110R'})
        },
        {
            "type": "postback",
            "title": "Itinerary",
            "payload": JSON.stringify({type:'itinerary', bookingNumber: 'D3WSA110R'})
        },
        {
            "type": "postback",
            "title": "Flight update",
            "payload": JSON.stringify({type:'update', bookingNumber: 'D3WSA110R'})
        },
        {
            "type": "web_url",
            "title": "Visit website",
            "url": MENU_URL
        }
        ]
  };
  request
      .post(url)
      .json(data)
      .on('response', (response) =>  {
          console.log("Cloud response:\n " + JSON.stringify(response))
      })
      .on('error', error => console.error(error));
};

exports.greeting = () => {
  let data = {};
  request
      .post(url)
      .json(data)
      .on('response',  response =>  console.log("Cloud response:\n " + JSON.stringify(response)))
      .on('error', error => console.error(error));
};

exports.getStarted = () => {
  let data = {};
  request
      .post(url)
      .json(data)
      .on('response',  response =>  console.log("Cloud response:\n " + response))
      .on('error', error => console.error(error));
};


