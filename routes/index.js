var router = require('express').Router();

var controllers = require('../controllers');


router
    .get('/', (req, res) => { res.end('Hola from Lenbot!')} )

    //facebook hooks
    .get('/fb/hook', controllers.subscribe)

    .post('/fb/hook', controllers.receive)

    //telegram hooks
    .post('/tg/hook', controllers.tgReceive);


module.exports = router;