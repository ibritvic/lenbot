var fb          = require('../facebook'),
    config      = require('config'),
    facebook  = config.get('facebook');

const VALIDATION_TOKEN = facebook.validationToken;

exports.subscribe = (req, res) => {
    if (req.query['hub.mode'] === 'subscribe' &&
        req.query['hub.verify_token'] === VALIDATION_TOKEN) {
        console.log("Validating webhook");
        res.status(200).send(req.query['hub.challenge']);
    } else {
        console.error("Failed validation. Make sure the validation tokens match.");
        res.sendStatus(403);
    }
};

exports.receive = (req, res) => {
    var data = req.body;
    console.log(JSON.stringify(req.body))
    // Make sure this is a page subscription
    if (data.object == 'page') {
        // Iterate over each entry
        // There may be multiple if batched
        for (pageEntry of data.entry){
            var pageID = pageEntry.id;
            var timeOfEvent = pageEntry.time;
            // Iterate over each messaging event
            for(messagingEvent of pageEntry.messaging) {
                if (messagingEvent.optin) {
                    fb.receivedAuthentication(messagingEvent);
                } else if (messagingEvent.message) {
                    fb.receivedMessage(messagingEvent);
                } else if (messagingEvent.delivery) {
                    fb.receivedDeliveryConfirmation(messagingEvent);
                } else if (messagingEvent.postback) {
                    fb.receivedPostback(messagingEvent);
                } else {
                    console.log("Webhook received unknown messagingEvent: ", messagingEvent);
                }
            }
        }

        // Assume all went well.
        //
        // You must send back a 200, within 20 seconds, to let us know you've
        // successfully received the callback. Otherwise, the request will time out.
        res.sendStatus(200);
    } else {
        res.sendStatus(400); //Not a fb page, what is it
    }
};

exports.tgReceive = (req, res) => {};

