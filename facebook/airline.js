"use strict";
const   fb =  require('./index')
    ,   bot = require('../bot');

/**
 * Airline fb messager bot
 * doing checkin and staff
 */
class Airline {
    constructor(messenger) {
        this.messenger = messenger; //inject fb messanger dependence

        //TODO: Async await or promises
        fb.on('message',(senderID, message)=>{
            console.log('Event message: ' + JSON.stringify(message));
            bot.reply(senderID, message.text.trim(), (err, reply)=> {
                if (err) return console.error(err); //TODO: Error logging
                let start, stop;
                if ((start = reply.indexOf('{')) != -1 && (stop = reply.indexOf('}')) != -1){ // Commands with {}
                    let commandStr = reply.substring(start + 1, stop);
                    let command = commandStr.split('-'); //TODO: hypen is separator,(space and underscore probs) not very nice
                    switch (command[0]){
                        case 'call':
                            this.sendCallButton(senderID, command[1], command[2]);
                            break;
                        case 'update':
                            this.sendUpdate(senderID, command[1]);
                            break;
                    }

                } else {
                    fb.sendTextMessage(senderID, reply);
                }

            });
        });

        this.messenger.on('postback',(senderID, recipientID, payload)=>{
            switch(payload.type){
                case 'checkin': //from persistent menu
                    //return to sender
                    this.sendCheckIn(senderID, payload.bookingNumber);
                    break;
                case 'boarding': //click on checkin reminder
                    //return to sender
                    this.sendBoardingPass(senderID, payload.bookingNumber);
                    break;
                case 'itinerary': //click on checkin reminder
                    //return to sender
                    this.sendItinerary(senderID);
                    break;
                case 'update': //click on flight update reminder
                    this.sendUpdate(senderID);
                    break;
            }
        });
    }

    /**
     * check in request
     * @param code
     */
    sendCheckIn(recipientID, bookingNumber ){

        let payload = {
            "template_type": "airline_checkin",
            "intro_message": "Check-in is available now.",
            "locale": "en_US",
            "pnr_number": bookingNumber,
            "flight_info": [
                {
                    "flight_number": "f001",
                    "departure_airport": {
                        "airport_code": "SFO",
                        "city": "San Francisco",
                        "terminal": "T4",
                        "gate": "G8"
                    },
                    "arrival_airport": {
                        "airport_code": "ZGB",
                        "city": "Zagreb",
                        "terminal": "T4",
                        "gate": "G8"
                    },
                    "flight_schedule": {
                        "boarding_time": "2016-01-05T15:05",
                        "departure_time": "2016-01-05T15:45",
                        "arrival_time": "2016-01-05T17:30"
                    }
                }
            ],
            "checkin_url": "http:\/\/www.croatiaairlines.com\/Plan-Book\/check-in\/Web-check-in" //"https:\/\/bot.lenitos.com\/fb\/hook\/"
           /* ,"buttons":[ //Doesn't work, pity
                {
                    "type":"postback",
                    "title":"Direct checkin",
                    "payload":JSON.stringify({ type:'checkIn', bookingNumber:bookingNumber })
                }
            ]*/
        };
        this.messenger.sendTemplate(recipientID, payload);
    }

    /**
     * Send boarding pass to customer
     * @param recepientID
     * @param bookingNumber
     */
    sendBoardingPass(recipientID, bookingNumber){
        let payload = {
            "template_type": "airline_boardingpass",
            "intro_message": "You are checked in.",
            "locale": "en_US",
            "boarding_pass": [
                {
                    "passenger_name": "SMITH\/NICOLAS",
                    "pnr_number": "CG4X7U",
                    "travel_class": "business",
                    "seat": "74J",
                    "auxiliary_fields": [
                        {
                            "label": "Terminal",
                            "value": "T1"
                        },
                        {
                            "label": "Departure",
                            "value": "30OCT 19:05"
                        }
                    ],
                    "secondary_fields": [
                        {
                            "label": "Boarding",
                            "value": "18:30"
                        },
                        {
                            "label": "Gate",
                            "value": "D57"
                        },
                        {
                            "label": "Seat",
                            "value": "74J"
                        },
                        {
                            "label": "Sec.Nr.",
                            "value": "003"
                        }
                    ],
                    "logo_image_url": "https:\/\/www.example.com\/en\/logo.png",
                    "header_image_url": "https:\/\/www.example.com\/en\/fb\/header.png",
                    "qr_code": "M1SMITH\/NICOLAS  CG4X7U nawouehgawgnapwi3jfa0wfh",
                    "above_bar_code_image_url": "https:\/\/www.example.com\/en\/PLAT.png",
                    "flight_info": {
                        "flight_number": "KL0642",
                        "departure_airport": {
                            "airport_code": "JFK",
                            "city": "New York",
                            "terminal": "T1",
                            "gate": "D57"
                        },
                        "arrival_airport": {
                            "airport_code": "AMS",
                            "city": "Amsterdam"
                        },
                        "flight_schedule": {
                            "departure_time": "2016-01-02T19:05",
                            "arrival_time": "2016-01-05T17:30"
                        }
                    }
                },
                {
                    "passenger_name": "VIKTOR\/MISEK",
                    "pnr_number": "CG4X7U",
                    "travel_class": "business",
                    "seat": "74K",
                    "auxiliary_fields": [
                        {
                            "label": "Terminal",
                            "value": "T1"
                        },
                        {
                            "label": "Departure",
                            "value": "30OCT 19:05"
                        }
                    ],
                    "secondary_fields": [
                        {
                            "label": "Boarding",
                            "value": "18:30"
                        },
                        {
                            "label": "Gate",
                            "value": "D57"
                        },
                        {
                            "label": "Seat",
                            "value": "74K"
                        },
                        {
                            "label": "Sec.Nr.",
                            "value": "004"
                        }
                    ],
                    "logo_image_url": "https:\/\/www.example.com\/en\/logo.png",
                    "header_image_url": "https:\/\/www.example.com\/en\/fb\/header.png",
                    "qr_code": "M1JONES\/FARBOUND  CG4X7U nawouehgawgnapwi3jfa0wfh",
                    "above_bar_code_image_url": "https:\/\/www.example.com\/en\/PLAT.png",
                    "flight_info": {
                        "flight_number": "KL0642",
                        "departure_airport": {
                            "airport_code": "JFK",
                            "city": "New York",
                            "terminal": "T1",
                            "gate": "D57"
                        },
                        "arrival_airport": {
                            "airport_code": "AMS",
                            "city": "Amsterdam"
                        },
                        "flight_schedule": {
                            "departure_time": "2016-01-02T19:05",
                            "arrival_time": "2016-01-05T17:30"
                        }
                    }
                }
            ]
        };
        this.messenger.sendTemplate(recipientID, payload);
    }

    /**
     * Name says it all
     * @param recipientID
     */
    sendItinerary(recipientID) {
        let payload = {
            "template_type": "airline_itinerary",
            "intro_message": "Here\'s your flight itinerary.",
            "locale": "en_US",
            "pnr_number": "ABCDEF",
            "passenger_info": [
                {
                    "name": "Johnny Gitara",
                    "ticket_number": "0741234567890",
                    "passenger_id": "p001"
                },
                {
                    "name": "Mara Palčić",
                    "ticket_number": "0741234567891",
                    "passenger_id": "p002"
                }
            ],
            "flight_info": [
                {
                    "connection_id": "c001",
                    "segment_id": "s001",
                    "flight_number": "KL9123",
                    "aircraft_type": "Boeing 737",
                    "departure_airport": {
                        "airport_code": "ZGB",
                        "city": "Zagreb",
                        "terminal": "T2",
                        "gate": "G8"
                    },
                    "arrival_airport": {
                        "airport_code": "AMS",
                        "city": "Amsterdam",
                        "terminal": "T4",
                        "gate": "G8"
                    },
                    "flight_schedule": {
                        "departure_time": "2016-01-02T19:45",
                        "arrival_time": "2016-01-02T21:20"
                    },
                    "travel_class": "business"
                },
                {
                    "connection_id": "c002",
                    "segment_id": "s002",
                    "flight_number": "KL321",
                    "aircraft_type": "Boeing 747-200",
                    "travel_class": "business",
                    "departure_airport": {
                        "airport_code": "SPU",
                        "city": "Split",
                        "terminal": "T1",
                        "gate": "G3"
                    },
                    "arrival_airport": {
                        "airport_code": "LON",
                        "city": "London",
                        "terminal": "T1",
                        "gate": "G33"
                    },
                    "flight_schedule": {
                        "departure_time": "2016-01-02T22:45",
                        "arrival_time": "2016-01-03T17:20"
                    }
                }
            ],
            "passenger_segment_info": [
                {
                    "segment_id": "s001",
                    "passenger_id": "p001",
                    "seat": "12A",
                    "seat_type": "Business"
                },
                {
                    "segment_id": "s001",
                    "passenger_id": "p002",
                    "seat": "12B",
                    "seat_type": "Business"
                },
                {
                    "segment_id": "s002",
                    "passenger_id": "p001",
                    "seat": "73A",
                    "seat_type": "World Business",
                    "product_info": [
                        {
                            "title": "Lounge",
                            "value": "Complimentary lounge access"
                        },
                        {
                            "title": "Baggage",
                            "value": "1 extra bag 50lbs"
                        }
                    ]
                },
                {
                    "segment_id": "s002",
                    "passenger_id": "p002",
                    "seat": "73B",
                    "seat_type": "World Business",
                    "product_info": [
                        {
                            "title": "Lounge",
                            "value": "Complimentary lounge access"
                        },
                        {
                            "title": "Baggage",
                            "value": "1 extra bag 50lbs"
                        }
                    ]
                }
            ],
            "price_info": [
                {
                    "title": "Fuel surcharge",
                    "amount": "123",
                    "currency": "EUR"
                }
            ],
            "base_price": "100",
            "tax": "23",
            "total_price": "123",
            "currency": "EUR"
        };

        this.messenger.sendTemplate(recipientID, payload);
    }

    /**
     * Flight update info
     * @param recipientID
     * @param text - template text message
     */
    sendUpdate(recipientID, text) {
        text = text || "Your flight is delayed";
        let payload = {
            "template_type": "airline_update",
            "intro_message": text,
            "update_type": "delay",
            "locale": "en_US",
            "pnr_number": "CF23G2",
            "update_flight_info": {
                "flight_number": "KL123",
                "departure_airport": {
                    "airport_code": "ZGB",
                    "city": "Zagreb",
                    "terminal": "T4",
                    "gate": "G8"
                },
                "arrival_airport": {
                    "airport_code": "AMS",
                    "city": "Amsterdam",
                    "terminal": "T4",
                    "gate": "G8"
                },
                "flight_schedule": {
                    "boarding_time": "2016-12-26T10:30",
                    "departure_time": "2016-12-26T11:30",
                    "arrival_time": "2016-12-27T07:30"
                }
            }
        };

        this.messenger.sendTemplate(recipientID, payload);
    }

    /**
     * Send button to call
     * @param code
     */
    sendCallButton(recipientID, callNumber, text ){
        let payload = {
            "template_type": "button",
            text,
            "buttons": [
                {
                    "type": "phone_number",
                    "title": "Phone call",
                    "payload": callNumber
                }
            ]
        };

        this.messenger.sendTemplate(recipientID, payload);
    }

}

module.exports = Airline;
