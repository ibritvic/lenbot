'use strict';

const EventEmitter = require('events'),
      request = require('request'),
      crypto = require('crypto'),
      config = require('config'),
      facebook  = config.get('facebook');

// App Secret CAN'T (!!!) be retrieved from the App Dashboard this is future option in that case watch config.get in other files
const APP_SECRET = (process.env.MESSENGER_APP_SECRET) ?
    process.env.MESSENGER_APP_SECRET :
    facebook.appSecret;

// Arbitrary value used to validate a webhook
const VALIDATION_TOKEN = (process.env.MESSENGER_VALIDATION_TOKEN) ?
    (process.env.MESSENGER_VALIDATION_TOKEN) :
    facebook.validationToken;

// Generate a page access token for your page from the App Dashboard
const PAGE_ACCESS_TOKEN = (process.env.MESSENGER_PAGE_ACCESS_TOKEN) ?
    (process.env.MESSENGER_PAGE_ACCESS_TOKEN) :
    facebook.pageAccessToken;

class Facebook extends EventEmitter {
    /**
     * tokens and reference to bot which provide answers
     */
    constructor () {
        super();


        if (!(APP_SECRET && VALIDATION_TOKEN && PAGE_ACCESS_TOKEN)) {
            throw new Error('Missing tokens and config values...');
        }

        this.token = PAGE_ACCESS_TOKEN;
        this.verify_token = VALIDATION_TOKEN;
        this.uri =  config.get('fbUri') || 'https://graph.facebook.com/v2.6/me/messages';
    }

    /**
     * Verify that the callback came from Facebook. Using the App Secret from
     * the App Dashboard, we can verify the signature that is sent with each
     * callback in the x-hub-signature field, located in the header.
     * https://developers.facebook.com/docs/graph-api/webhooks#setup
     *
     */
    verifyRequestSignature(req, res, buf) {
        if (req.url.indexOf('/fb/') < 0 ) return; //only check for facebook

        var signature = req.headers["x-hub-signature"];

        if (!signature) {
            // For testing, let's log an error. In production, you should throw an
            // error.
            //throw new Error("Couldn't validate the request signature.");
            console.error("Couldn't validate the signature.");
        } else {
            var elements = signature.split('=');
            var method = elements[0];
            var signatureHash = elements[1];

            var expectedHash = crypto.createHmac('sha1', APP_SECRET)//called from expressjs, this don't work
                .update(buf)
                .digest('hex');

            if (signatureHash != expectedHash) {
                throw new Error("Couldn't validate the request signature.");
            }
        }
    }

    /*
     * Authorization Event
     *
     * The value for 'optin.ref' is defined in the entry point. For the "Send to
     * Messenger" plugin, it is the 'data-ref' field. Read more at
     * https://developers.facebook.com/docs/messenger-platform/webhook-reference#auth
     *
     */
    receivedAuthentication(event){
        var senderID = event.sender.id;
        var recipientID = event.recipient.id;
        var timeOfAuth = event.timestamp;

        // The 'ref' field is set in the 'Send to Messenger' plugin, in the 'data-ref'
        // The developer can set this to an arbitrary value to associate the
        // authentication callback with the 'Send to Messenger' click event. This is
        // a way to do account linking when the user clicks the 'Send to Messenger'
        // plugin.
        var passThroughParam = event.optin.ref;

        console.log("Received authentication for user %d and page %d with pass " +
            "through param '%s' at %d", senderID, recipientID, passThroughParam,
            timeOfAuth);

        // When an authentication is received, we'll send a message back to the sender
        // to let them know it was successful.
        this.sendTextMessage(senderID, "Authentication successful");
    }

    /*
     * Message Event
     *
     * This event is called when a message is sent to your page. The 'message'
     * object format can vary depending on the kind of message that was received.
     * Read more at https://developers.facebook.com/docs/messenger-platform/webhook-reference#received_message
     *
     * For this example, we're going to echo any text that we get. If we get some
     * special keywords ('button', 'generic', 'receipt'), then we'll send back
     * examples of those bubbles to illustrate the special message bubbles we've
     * created. If we receive a message with an attachment (image, video, audio),
     * then we'll simply confirm that we've received the attachment.
     *
     */
    receivedMessage(event) {
        var senderID = event.sender.id;
        var recipientID = event.recipient.id;
        var timeOfMessage = event.timestamp;
        var message = event.message;

        console.log("Received message for user %d and page %d at %d with message:",
            senderID, recipientID, timeOfMessage);
        console.log(JSON.stringify(message));

        var messageId = message.mid;

        // You may get a text or attachment but not both
        var messageText = message.text;
        var messageAttachments = message.attachments;
        this.emit('message', senderID, message);
    }

    /*
     * Delivery Confirmation Event
     *
     * This event is sent to confirm the delivery of a message. Read more about
     * these fields at https://developers.facebook.com/docs/messenger-platform/webhook-reference#message_delivery
     *
     */
    receivedDeliveryConfirmation(event) {
        var senderID = event.sender.id;
        var recipientID = event.recipient.id;
        var delivery = event.delivery;
        var messageIDs = delivery.mids;
        var watermark = delivery.watermark;
        var sequenceNumber = delivery.seq;

        if (messageIDs) {
            messageIDs.forEach(function(messageID) {
                console.log("Received delivery confirmation for message ID: %s",
                    messageID);
            });
        }

        console.log("All message before %d were delivered.", watermark);
    }


    /*
     * Postback Event
     *
     * This event is called when a postback is tapped on a Structured Message. Read
     * more at https://developers.facebook.com/docs/messenger-platform/webhook-reference#postback
     *
     */
    receivedPostback (event){
        var senderID = event.sender.id;
        var recipientID = event.recipient.id;
        var timeOfPostback = event.timestamp;

        // The 'payload' param is a developer-defined field which is set in a postback
        // button for Structured Messages.
        var payload = event.postback.payload;



        console.log("Received postback for user %d and page %d with payload '%s' " +
            "at %d", senderID, recipientID,  payload, timeOfPostback);


        this.emit('postback', senderID, recipientID, JSON.parse(payload));
        // When a postback is called, we'll send a message back to the sender to
        // let them know it was successful
        //this.sendTextMessage(senderID, "Postback called");
    }

    sendTextMessage(recipientId, messageText) {
        var messageData = {
            recipient: {
                id: recipientId
            },
            message: {
                text: messageText
            }
        };

        this._callSendAPI(messageData);
    }

    /**
     * Prepares and sends template to messenger
     * @param recipientId
     * @param data
     */
    sendTemplate(recipientId, payload) {
        var messageData = {
            recipient: {
                id: recipientId
            },
            message: {
                attachment: {
                    "type": "template",
                    "payload": payload
                }
            }
        };

        this._callSendAPI(messageData);
    }


    /*
     * Call the Send API. The message data goes in the body. If successful, we'll
     * get the message id in a response
     *
     */
    _callSendAPI(messageData) {
        request({
            uri: this.uri,
            qs: {access_token: this.token},
            method: 'POST',
            json: messageData
        },  (error, response, body) => {

            if (!error && response.statusCode == 200) {
                var recipientId = body.recipient_id;
                var messageId = body.message_id;

                console.log("Successfully sent generic message with id %s to recipient %s", messageId, recipientId);
                console.log(JSON.stringify(messageData));
            } else {
                console.error("Unable to send message.");
                console.error(response);
                console.error(error);
            }
        })
    }
}
const fb = new Facebook();

//Not a true singleton: https://derickbailey.com/2016/03/09/creating-a-true-singleton-in-node-js-with-es6-symbols/
module.exports = fb;